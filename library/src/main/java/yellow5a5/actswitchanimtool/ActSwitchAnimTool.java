package yellow5a5.actswitchanimtool;


import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;

/**
 * Created by Yellow5A5 on 16/9/16.
 */
public class ActSwitchAnimTool {

    public final static int ANIM_DURATION = 700;
    public final static int MODE_UNINIT = -1;
    public final static int MODE_SPREAD = 0;
    public final static int MODE_SHRINK = 1;

    private final String TRANSFORMER_COLOR_END = "ACT_SWITCH_ANIM_COLOR_END";
    private final String TRANSFORMER_TARGET_LOCATION = "ACT_SWITCH_ANIM_TARGET_LOCATION";

    private ComponentContainer mDecorView;
    private SwitchAnimView mSwitchAnimView;
    private Ability mStartAct;
    private int mColorStart;
    private int mColorEnd;

    private int targetViewWidth;
    private int targetViewHeight;
    private int[] targetLocation = new int[2];

    private boolean isNeedShrinkBack;
    private boolean isWaitingResume;

    public ActSwitchAnimTool(Ability activity, ComponentContainer componentContainer) {
        mStartAct = activity;
        mSwitchAnimView = new SwitchAnimView(mStartAct);
        mDecorView = componentContainer;
    }


    public ActSwitchAnimTool startActivity(final Intent intent, final boolean isNeedFinish) {
        intent.setParam(TRANSFORMER_TARGET_LOCATION, targetLocation);
        intent.setParam(TRANSFORMER_COLOR_END, mColorEnd);
        mSwitchAnimView.setSwitchAnimCallback(new SwitchAnimCallback() {
            @Override
            public void onAnimationStart() {
                switch (mSwitchAnimView.getmAnimType()) {
                    case MODE_SPREAD:
                        mSwitchAnimView.setClickable(true);
                        break;
                    case MODE_SHRINK:
                        mSwitchAnimView.setClickable(false);
                        break;
                }
            }

            @Override
            public void onAnimationEnd() {
                switch (mSwitchAnimView.getmAnimType()) {
                    case MODE_SPREAD:
                        mStartAct.startAbility(intent);
                        if (isNeedFinish == true) {
                            mStartAct.terminateAbility();
                        } else {
                            mStartAct.getUITaskDispatcher().delayDispatch(new Runnable() {
                                @Override
                                public void run() {
                                    //if you need turn back by shrinking~ you need add~
                                    if (isNeedShrinkBack == false) {
                                        mSwitchAnimView.resetAnimParam();
                                        mDecorView.removeComponent(mSwitchAnimView);
                                    } else {
                                        mSwitchAnimView.resetAnimParam();
                                        isWaitingResume = true;
                                    }
                                }
                            }, 200);
                        }
                        break;

                    case MODE_SHRINK:

                        break;
                }

            }

            @Override
            public void onAnimationUpdate(float progress) {
                /* Maybe you want change the alpha of Target-Component ~~.
                if (mTargetView != null){
                    mTargetView.setAlpha((float) (1 - progress / 100.0f));
                }
                */
            }
        });
        return this;
    }


    public ActSwitchAnimTool receiveIntent(Intent intent) {
        targetLocation = intent.getIntArrayParam(TRANSFORMER_TARGET_LOCATION);
        mColorEnd = intent.getIntParam(TRANSFORMER_COLOR_END, Color.BLUE.getValue());
        setmColorEnd(mColorEnd);
        return this;
    }

    public ActSwitchAnimTool target(final Component view) {
        final ComponentContainer.LayoutConfig bgParams = new ComponentContainer.LayoutConfig(
                ComponentContainer.LayoutConfig.MATCH_PARENT, ComponentContainer.LayoutConfig.MATCH_PARENT);
        //background image.
        mDecorView.addComponent(mSwitchAnimView, bgParams);
        view.setLayoutRefreshedListener(new Component.LayoutRefreshedListener() {
            @Override
            public void onRefreshed(Component component) {
                ComponentContainer p = ((ComponentContainer) view.getComponentParent());
                int h = p.getHeight();
                int w = p.getWidth();
                targetLocation = new int[]{getParentLeft(view.getLeft(), view), getParentTop(view.getTop(), view)};/*new int[]{view.getLeft(), view.getBottom()};*/ /*view.getLocationOnScreen();*/
                targetViewWidth = view.getWidth();
                targetViewHeight = view.getHeight();
                int circleRadius = (targetViewHeight > targetViewWidth ? targetViewWidth : targetViewHeight) / 2;
                mSwitchAnimView.setmTargetCircleRadius(circleRadius);
                mSwitchAnimView.setCenter(ActSwitchAnimTool.this.targetLocation[0] + targetViewWidth / 2, targetLocation[1] + targetViewHeight / 2);
            }
        });
        return this;
    }

    public static ComponentContainer findRoot(Component component) {
        while(component.getComponentParent() != null) {
            component = (Component)component.getComponentParent();
        }
        return (ComponentContainer)component;
    }

    public ActSwitchAnimTool setCustomEndCallBack(SwitchAnimCallback callback) {
        mSwitchAnimView.setSwitchAnimCallback(callback);
        return this;
    }

    public ActSwitchAnimTool addContainerView(final Component view, final SwitchAnimCallback callback) {
        mSwitchAnimView.setSwitchAnimCallback(new SwitchAnimCallback() {
            @Override
            public void onAnimationStart() {
                switch (mSwitchAnimView.getmAnimType()) {
                    case MODE_SPREAD:
                        mSwitchAnimView.setClickable(true);
                        break;

                    case MODE_SHRINK:
                        mSwitchAnimView.setClickable(false);
                        break;
                }
            }

            @Override
            public void onAnimationEnd() {
                if (view.getComponentParent() != null)
                    return;
                switch (mSwitchAnimView.getmAnimType()) {
                    case MODE_SPREAD:
                        mDecorView.addComponent(view);
                        callback.onAnimationEnd();
                        break;

                    case MODE_SHRINK:
                        mSwitchAnimView.setSwitchAnimCallback(null);
                        break;
                }
            }

            @Override
            public void onAnimationUpdate(float progress) {
                callback.onAnimationUpdate(progress);
            }
        });
        return this;
    }

    public ActSwitchAnimTool removeContainerView(Component view) {
        if (mDecorView != null && view != null) {
            mDecorView.removeComponent(view);
        }
        return this;
    }

    public void build() {
        if (mSwitchAnimView.getmAnimType() == MODE_SPREAD) {
            mSwitchAnimView.startSpreadAnim();
        } else if (mSwitchAnimView.getmAnimType() == MODE_SHRINK) {
            mSwitchAnimView.startShrinkAnim();
        }
    }

    public ActSwitchAnimTool setAnimType(int type) {
        mSwitchAnimView.setmAnimType(type);
        return this;
    }

    public ActSwitchAnimTool setmColorStart(int color) {
        mColorStart = color;
        mSwitchAnimView.setmSpreadColor(color);
        return this;
    }

    public ActSwitchAnimTool setmColorEnd(int color) {
        mColorEnd = color;
        mSwitchAnimView.setmShrinkColor(color);
        return this;
    }

    public ActSwitchAnimTool setShrinkBack(boolean isShrinkBack) {
        isNeedShrinkBack = isShrinkBack;
        return this;
    }

    public boolean isShrinkBack() {
        return isNeedShrinkBack;
    }

    public boolean isWaitingResume() {
        return isWaitingResume;
    }

    public ActSwitchAnimTool setIsWaitingResume(boolean isWaitingResume) {
        this.isWaitingResume = isWaitingResume;
        return this;
    }

    public int getTargetViewWidth() {
        return targetViewWidth;
    }

    public void setTargetViewWidth(int targetViewWidth) {
        this.targetViewWidth = targetViewWidth;
    }

    public int getTargetViewHeight() {
        return targetViewHeight;
    }

    public void setTargetViewHeight(int targetViewHeight) {
        this.targetViewHeight = targetViewHeight;
    }

    public interface SwitchAnimCallback {
        void onAnimationStart();

        void onAnimationEnd();

        void onAnimationUpdate(float progress);
    }

    public class SwitchAnimView extends Component implements Component.DrawTask {

        private int mCenterX;
        private int mCenterY;
        private int mSpreadColor;
        private int mShrinkColor;
        private int mAnimType;
        private float mRadius;
        private int mDuration;
        private Paint mSpreadPaint;
        private Paint mShrinkPaint;
        private AnimatorValue mAnimator;
        private int mTargetCircleRadius;
        private int mScreenLength;


        private boolean isAnimationReady = false;

        private SwitchAnimCallback mSwitchAnimCallback;

        public void setSwitchAnimCallback(SwitchAnimCallback callback) {
            mSwitchAnimCallback = callback;
        }

        public SwitchAnimView(Context context) {
            this(context, null);
        }

        public SwitchAnimView(Context context, AttrSet attrs) {
            this(context, attrs, 0);
        }

        public SwitchAnimView(Context context, AttrSet attrs, int defStyle) {
            super(context, attrs, defStyle);
            init(context);
            initAnimation();
            addDrawTask(this::onDraw);
        }


        private void init(Context context) {
            mAnimType = MODE_UNINIT;
            mSpreadPaint = new Paint();
            mSpreadPaint.setStrokeWidth(1);
            mSpreadPaint.setStyle(Paint.Style.STROKE_STYLE);
            mSpreadPaint.setColor(Color.BLUE);
            mShrinkPaint = new Paint();
            mShrinkPaint.setStrokeWidth(1);
            mShrinkPaint.setStyle(Paint.Style.STROKE_STYLE);
            mShrinkPaint.setColor(Color.BLUE);
            mDuration = ANIM_DURATION;
            mScreenLength = (int) Math.sqrt(Math.pow(DisplayManager.getInstance().getDefaultDisplay(context).get().getAttributes().height
                    /*context.getResourceManager().getDeviceCapability().height*/
                    , 2)
                    + Math.pow(DisplayManager.getInstance().getDefaultDisplay(context).get().getAttributes().width/*context.getResourceManager().getDeviceCapability().width*/, 2));

        }

        public void resetAnimParam() {
            mAnimType = -1;
            mRadius = 0;
            mSpreadPaint.setStrokeWidth(mRadius);
            mShrinkPaint.setStrokeWidth(mRadius);
            invalidate();
        }

         public    boolean flag;
        private void initAnimation() {
            isAnimationReady = true;
            mAnimator = new AnimatorValue();
            mAnimator.setDuration(mDuration);
            mAnimator.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
                float lastFactor = 0.0f;

                @Override
                public void onUpdate(AnimatorValue animatorValue, float v) {
                        float maxFactor =  v * mScreenLength;
                    if ((int)maxFactor ==mScreenLength|| flag==true){
                        flag=true;
                    }else {
                        float factor1 = v * mScreenLength;
                        mRadius = factor1;
                        invalidate();
                        if (mSwitchAnimCallback != null && lastFactor != factor1) {
                            mSwitchAnimCallback.onAnimationUpdate(factor1 * 100 / mScreenLength);
                            lastFactor = factor1;
                        }
                    }
                }
            });
            mAnimator.setStateChangedListener(new Animator.StateChangedListener() {
                @Override
                public void onStart(Animator animator) {
                    if (mSwitchAnimCallback != null) {
                        mSwitchAnimCallback.onAnimationStart();
                        if (mAnimType == MODE_SPREAD) {
                            setVisibility(VISIBLE);
                        }
                    }
                }

                @Override
                public void onStop(Animator animator) {

                }

                @Override
                public void onCancel(Animator animator) {

                }

                @Override
                public void onEnd(Animator animator) {
                    if (mSwitchAnimCallback != null) {
                        mSwitchAnimCallback.onAnimationEnd();
                        if (mAnimType == MODE_SHRINK) {
                            setVisibility(HIDE);
                        }
                    }
                }

                @Override
                public void onPause(Animator animator) {

                }

                @Override
                public void onResume(Animator animator) {

                }
            });
        }

        private void startSpreadAnim() {
            mAnimator.start();
        }

        private void startShrinkAnim() {
            mAnimator.setDuration(mDuration);
            mAnimator.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
                    float lastFactor=0.0f;
                @Override
                public void onUpdate(AnimatorValue animatorValue, float v) {
                    float min = (1-v) * mScreenLength;
                    float factor = Math.min(min, mScreenLength);
                    mRadius = factor;
                    invalidate();
                    if (mSwitchAnimCallback != null && lastFactor != factor) {
                        mSwitchAnimCallback.onAnimationUpdate(factor * 100 / mScreenLength);
                        lastFactor = factor;
                    }
                }
            });
            mAnimator.start();
        }

        public void setCenter(int centerX, int centerY) {
            mCenterX = centerX;
            mCenterY = centerY;
        }

        @Override
        public void onDraw(Component component, Canvas canvas) {
            if (getmAnimType() == MODE_UNINIT) {
                return;
            }
            if (!isAnimationReady&& flag==false) {
                initAnimation();
            }

            if (flag){
                startShrinkAnim();
            }
            switch (getmAnimType()) {
                case MODE_SPREAD:
                    mSpreadPaint.setStrokeWidth(mRadius);
                    canvas.drawCircle(mCenterX, mCenterY, mRadius / 2 + mTargetCircleRadius, mSpreadPaint);
                    break;
                case MODE_SHRINK:
                    mShrinkPaint.setStrokeWidth(mRadius);
                    canvas.drawCircle(mCenterX, mCenterY, mRadius / 2 + mTargetCircleRadius, mShrinkPaint);
                    break;
            }

        }

        private void setAlpha(int alpha) {
            mSpreadPaint.setAlpha(alpha);
            mShrinkPaint.setAlpha(alpha);
        }

        public float getmRadius() {
            return mRadius;
        }

        public void setmRadius(int mRadius) {
            this.mRadius = mRadius;
        }

        public int getmDuration() {
            return mDuration;
        }

        public void setmDuration(int mDuration) {
            this.mDuration = mDuration;
            isAnimationReady = false;
        }

        public int getmSpreadColor() {
            return mSpreadColor;
        }

        public void setmSpreadColor(int color) {
            mSpreadPaint.setColor(new Color(color));
            this.mSpreadColor = color;
        }

        public int getmShrinkColor() {
            return mShrinkColor;
        }

        public void setmShrinkColor(int color) {
            mShrinkPaint.setColor(new Color(color));
            this.mShrinkColor = color;
        }

        public int getmAnimType() {
            return mAnimType;
        }

        public void setmAnimType(int mAnimType) {
            this.mAnimType = mAnimType;
        }

        public int getmTargetCircleRadius() {
            return mTargetCircleRadius;
        }

        public void setmTargetCircleRadius(int mTargetCircleRadius) {
            this.mTargetCircleRadius = mTargetCircleRadius;
        }


    }

    public static int getParentTop(int top, Component c) {

        if (c != null) {
            if (c.getComponentParent() != null) {
                ComponentContainer parent = (ComponentContainer) c.getComponentParent();
                if (parent != null) {
                    top += parent.getTop() + parent.getPaddingTop();
                    top = getParentTop(top, parent);
                }
            }
        }

        return top;
    }

    public static int getParentLeft(int left, Component c) {

        if (c != null) {
            if (c.getComponentParent() != null) {
                ComponentContainer parent = (ComponentContainer) c.getComponentParent();
                if (parent != null) {
                    left += parent.getLeft() + parent.getPaddingLeft();
                    left = getParentLeft(left, parent);
                }
            }
        }

        return left;
    }
}

