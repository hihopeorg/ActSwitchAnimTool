/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package yellow5a5.sample;

import ohos.agp.utils.Color;
import org.junit.Assert;
import org.junit.Test;
import yellow5a5.actswitchanimtool.ActSwitchAnimTool;

import java.lang.reflect.Method;

public class ExampleTest {
    @Test
    public void onStart() {
    }

    @Test
    public void setShrinkBack() {
        Class<ActSwitchAnimTool> actSwitchAnimToolClass = ActSwitchAnimTool.class;
        try {
            Method method = actSwitchAnimToolClass.getMethod("setShrinkBack");
            ActSwitchAnimTool invoke = (ActSwitchAnimTool) method.invoke(actSwitchAnimToolClass);
            invoke.setShrinkBack(true);
            Assert.assertTrue(invoke.isShrinkBack());
        }catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }  catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Test
    public  void setIsWaitingResume() {
        Class<ActSwitchAnimTool> actSwitchAnimToolClass = ActSwitchAnimTool.class;
        try {
            Method method = actSwitchAnimToolClass.getMethod("setIsWaitingResume");
            ActSwitchAnimTool invoke = (ActSwitchAnimTool) method.invoke(actSwitchAnimToolClass);
            invoke.setIsWaitingResume(true);
            Assert.assertTrue(invoke.isWaitingResume());
        }catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }  catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public  void setmColorStart() {
        Class<ActSwitchAnimTool> actSwitchAnimToolClass = ActSwitchAnimTool.class;
        try {
            Method method = actSwitchAnimToolClass.getMethod("setmColorStart");
            ActSwitchAnimTool invoke = (ActSwitchAnimTool) method.invoke(actSwitchAnimToolClass);
            invoke.setmColorStart(Color.RED.getValue());

            Class<ActSwitchAnimTool.SwitchAnimView> switchanimview = ActSwitchAnimTool.SwitchAnimView.class;
            Method method1 = switchanimview.getMethod("getmSpreadColor");
            ActSwitchAnimTool.SwitchAnimView invoke1 = (ActSwitchAnimTool.SwitchAnimView)method1.invoke(switchanimview);

            Assert.assertTrue(invoke1.getmSpreadColor()==Color.RED.getValue());
        }catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }  catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Test
    public  void setmColorEnd() {
        Class<ActSwitchAnimTool> actSwitchAnimToolClass = ActSwitchAnimTool.class;
        try {

            Method method = actSwitchAnimToolClass.getMethod("setmColorEnd");
            ActSwitchAnimTool invoke = (ActSwitchAnimTool) method.invoke(actSwitchAnimToolClass);
            invoke.setmColorEnd(Color.RED.getValue());

            Class<ActSwitchAnimTool.SwitchAnimView> switchanimview = ActSwitchAnimTool.SwitchAnimView.class;
            Method method1 = switchanimview.getMethod("getmShrinkColor");
            ActSwitchAnimTool.SwitchAnimView invoke1 = (ActSwitchAnimTool.SwitchAnimView)method1.invoke(switchanimview);

            Assert.assertTrue(invoke1.getmShrinkColor()==Color.RED.getValue());
        }catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }  catch (Exception e) {
            e.printStackTrace();
        }
    }

}
