package yellow5a5.sample;


import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.colors.RgbPalette;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.Image;
import ohos.agp.components.Text;
import ohos.multimodalinput.event.KeyEvent;
import yellow5a5.actswitchanimtool.ActSwitchAnimTool;

public class SecondActivity extends Ability {

    private Text mDemoTv;
    private Image mDemoFloatingBtn;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_activity_second);
        mDemoTv = (Text) findComponentById(ResourceTable.Id_demo_tv);
        mDemoFloatingBtn = (Image) findComponentById(ResourceTable.Id_demo_float_btn);
        new ActSwitchAnimTool(SecondActivity.this,findRoot(findComponentById(ResourceTable.Id_demo_float_btn)))
                .receiveIntent(getIntent())
                .setAnimType(ActSwitchAnimTool.MODE_SHRINK)
                .target(mDemoFloatingBtn)
                .build();
    }
    public static ComponentContainer findRoot(Component component) {
            component = (Component)component.getComponentParent();
        return (ComponentContainer)component;
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEY_BACK){
            new ActSwitchAnimTool(SecondActivity.this,findRoot(findComponentById(ResourceTable.Id_demo_float_btn)))
                    .setAnimType(ActSwitchAnimTool.MODE_SPREAD)
                    .target(mDemoFloatingBtn)
                    .setmColorStart(RgbPalette.parse("#FF5777"))
                    .setmColorEnd(RgbPalette.parse("#FF5777"))
                    .setCustomEndCallBack(new ActSwitchAnimTool.SwitchAnimCallback() {
                        @Override
                        public void onAnimationStart() {
                        }

                        @Override
                        public void onAnimationEnd() {
                            terminateAbility();
                        }

                        @Override
                        public void onAnimationUpdate(float progress) {

                        }
                    })
                    .build();
            return true;
        }
        return super.onKeyUp(keyCode, event);
    }
}
