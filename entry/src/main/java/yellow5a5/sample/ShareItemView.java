package yellow5a5.sample;


import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.*;
import ohos.agp.utils.TextTool;
import ohos.app.Context;
import yellow5a5.sample.util.AttrUtils;

/**
 * Created by Yellow5A5 on 16/9/17.
 */
public class ShareItemView extends DependentLayout {

    private int ANIM_START = 0;
    private int ANIM_REVERSE = 1;

    private int mAnimMode = 0;

    private Image mImageV;
    private Text mTextV;

    private AnimatorValue animator;


    public ShareItemView(Context context, AttrSet attrs) {
        this(context, attrs, "");
    }

    public ShareItemView(Context context, AttrSet attrs, String defStyle) {
        super(context, attrs, defStyle);
        int imgId = AttrUtils.getIdFromAttr(attrs,"share_img",0);
        String text = attrs.getAttr("share_text").get().getStringValue();
        LayoutScatter.getInstance(context).parse(ResourceTable.Layout_share_item,this,true);
        mImageV = (Image) findComponentById(ResourceTable.Id_img_left);
        mTextV = (Text) findComponentById(ResourceTable.Id_tv_right);
        setImageShare(imgId);
        setTextShare(text);
        initAnimation();
        setAlpha(0);
    }

    private void initAnimation() {
        animator=new AnimatorValue();
        animator.setDuration(300);
        animator.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
            @Override
            public void onUpdate(AnimatorValue animatorValue, float v) {
                float factor = v;
                setTranslationY((1 - factor) * 100);
                setAlpha(factor);
            }
        });
        animator.setStateChangedListener(new Animator.StateChangedListener() {
            @Override
            public void onStart(Animator animator) {

            }

            @Override
            public void onStop(Animator animator) {

            }

            @Override
            public void onCancel(Animator animator) {

            }

            @Override
            public void onEnd(Animator animator) {
                if(mAnimMode == ANIM_START) {

                }else if(mAnimMode == ANIM_REVERSE){
                    setVisibility(HIDE);
                }
            }

            @Override
            public void onPause(Animator animator) {

            }

            @Override
            public void onResume(Animator animator) {

            }
        });
    }

    public void setTextShare(String text) {
        if (!TextTool.isNullOrEmpty(text)) {
            mTextV.setText(text);
        }
    }

    public void setImageShare(int resId) {
        if (resId != 0) {
            mImageV.setImageAndDecodeBounds(resId);
        }
    }

    public void showAnimation() {
        setVisibility(Component.VISIBLE);
        mAnimMode = 0;
        animator.start();
    }

    public void hideAnimation() {
        mAnimMode = 1;
        animator.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
            @Override
            public void onUpdate(AnimatorValue animatorValue, float v) {
                float factor = v;
                setTranslationY(-((1 - factor) * 100));
                setAlpha(factor);
            }
        });
    }
}
