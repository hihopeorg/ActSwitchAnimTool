package yellow5a5.sample;


import ohos.aafwk.ability.OnClickListener;
import ohos.agp.components.AttrSet;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.Image;
import ohos.agp.components.LayoutScatter;
import ohos.app.Context;
import ohos.app.dispatcher.TaskDispatcher;
import ohos.app.dispatcher.task.TaskPriority;

/**
 * Created by Yellow5A5 on 16/9/17.
 */
public class ShareContainer extends DirectionalLayout implements IShare {


    private ShareItemView mFaceBookItem;
    private ShareItemView mTwitterItem;
    private ShareItemView mGooglePlusItem;
    private Image mCancelV;

    public interface IShareCallback {
        void onCancel();
    }

    private IShareCallback mIShareCallback;

    public void setIShareCallback(IShareCallback l) {
        mIShareCallback = l;
    }

    public ShareContainer(Context context) {
        this(context, null);
    }

    public ShareContainer(Context context, AttrSet attrs) {
        this(context, attrs, "");
    }

    public ShareContainer(Context context, AttrSet attrs, String defStyle) {
        super(context, attrs, defStyle);
        LayoutScatter.getInstance(context).parse(ResourceTable.Layout_share_container_layout,this,true);
        init();
    }

    private void init() {
        mFaceBookItem = (ShareItemView) findComponentById(ResourceTable.Id_item_facebook);
        mTwitterItem = (ShareItemView) findComponentById(ResourceTable.Id_item_twitter);
        mGooglePlusItem = (ShareItemView) findComponentById(ResourceTable.Id_item_google);
        mCancelV = (Image) findComponentById(ResourceTable.Id_img_cancel);
        mCancelV.setClickedListener((v)->{
            if (mIShareCallback != null) {
                mIShareCallback.onCancel();
            }
        });
    }

    public void showShareBtn() {
        mFaceBookItem.setVisibility(VISIBLE);
        mTwitterItem.setVisibility(VISIBLE);
        mGooglePlusItem.setVisibility(VISIBLE);
        String dispatcherName = "parallelTaskDispatcher";
        TaskDispatcher postDelayed = getContext().createParallelTaskDispatcher(dispatcherName, TaskPriority.DEFAULT);
        postDelayed.delayDispatch(new Runnable() {
            @Override
            public void run() {
                mFaceBookItem.showAnimation();
            }
        },100);

        postDelayed.delayDispatch(new Runnable() {
            @Override
            public void run() {
                mTwitterItem.showAnimation();
            }
        }, 200);
        postDelayed.delayDispatch(new Runnable() {
            @Override
            public void run() {
                mGooglePlusItem.showAnimation();
            }
        }, 300);
    }



    public void hideShareBtn(){
        mFaceBookItem.hideAnimation();
        mTwitterItem.hideAnimation();
        mGooglePlusItem.hideAnimation();
    }

    @Override
    public void faceBookShareClick() {

    }

    @Override
    public void twitterShareClick() {

    }

    @Override
    public void googlePlusShareClick() {

    }

}
