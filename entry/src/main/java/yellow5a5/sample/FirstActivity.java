package yellow5a5.sample;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.colors.RgbPalette;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.Image;
import ohos.agp.components.Text;
import ohos.agp.window.dialog.ToastDialog;
import ohos.multimodalinput.event.TouchEvent;
import yellow5a5.actswitchanimtool.ActSwitchAnimTool;


public class FirstActivity extends Ability {

    private Image mShareViewDemoBtn;
    private Image mActSwitchDemoBtn;
    private Text text;
    private ActSwitchAnimTool mFirstDemoActSwitchAnimTool;

    private ShareContainer mShareContainer;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_activity_first);
        mActSwitchDemoBtn = (Image) findComponentById(ResourceTable.Id_fab);
        text = (Text) findComponentById(ResourceTable.Id_text_title);

        //DEMO FIRST.
        initTool();
        mActSwitchDemoBtn.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                new ToastDialog(getContext()).setText("Replace with your own action").setDuration(1000).show();
                mFirstDemoActSwitchAnimTool.setAnimType(0).build();
            }
        });

        //DEMO SECOND.
        mShareViewDemoBtn = (Image) findComponentById(ResourceTable.Id_share_float_btn);
        final ActSwitchAnimTool shareDemoTool = new ActSwitchAnimTool(FirstActivity.this,findRoot(findComponentById(ResourceTable.Id_share_float_btn)))
                .target(mShareViewDemoBtn)
                .setmColorStart(RgbPalette.parse("#33D1FF"))
                .setmColorEnd(RgbPalette.parse("#33D1FF"));
        mShareContainer = new ShareContainer(FirstActivity.this);
        mShareContainer.setIShareCallback(new ShareContainer.IShareCallback() {
            @Override
            public void onCancel() {
                mShareContainer.hideShareBtn();
                shareDemoTool.setAnimType(ActSwitchAnimTool.MODE_SHRINK)
                            .removeContainerView(mShareContainer)
                            .build();
            }
        });
        mShareViewDemoBtn.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                shareDemoTool.setAnimType(ActSwitchAnimTool.MODE_SPREAD)
                        .addContainerView(mShareContainer, new ActSwitchAnimTool.SwitchAnimCallback() {
                            @Override
                            public void onAnimationStart() {}

                            @Override
                            public void onAnimationEnd() {
                                mShareContainer.showShareBtn();
                            }

                            @Override
                            public void onAnimationUpdate(float progress) {

                            }
                        }).
                        build();
            }
        });
    }

    private void initTool() {
        Intent intent = new Intent();
        Operation operation = new Intent.OperationBuilder()
                .withDeviceId("")
                .withBundleName("yellow5a5.sample")
                .withAbilityName("yellow5a5.sample.SecondActivity")
                .build();
        intent.setOperation(operation);
        mFirstDemoActSwitchAnimTool = new ActSwitchAnimTool(FirstActivity.this,findRoot(findComponentById(ResourceTable.Id_fab))).setAnimType(0)
                .target(mActSwitchDemoBtn)
                .setShrinkBack(true)
                .setmColorStart(RgbPalette.parse("#FF5777"))
                .setmColorEnd(RgbPalette.parse("#FF5777"))
                .startActivity(intent,false);

    }
    public static ComponentContainer findRoot(Component component) {
        while(component.getComponentParent() != null) {
            component = (Component)component.getComponentParent();
        }
        return (ComponentContainer)component;
    }


    @Override
    protected void onActive() {
        if (mFirstDemoActSwitchAnimTool == null)
            return;
        if (mFirstDemoActSwitchAnimTool.isWaitingResume()) {
            mFirstDemoActSwitchAnimTool.setAnimType(1)
                    .setIsWaitingResume(false)
                    .build();
        }
        super.onActive();
    }

}
