/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package yellow5a5.sample;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.aafwk.ability.delegation.IAbilityDelegator;
import ohos.agp.components.Component;
import ohos.agp.components.Image;
import ohos.eventhandler.EventHandler;
import ohos.multimodalinput.event.KeyEvent;
import org.junit.Assert;
import org.junit.Test;
import yellow5a5.actswitchanimtool.ActSwitchAnimTool;
import yellow5a5.sample.utile.EventHelper;

import java.lang.reflect.Method;

import static org.junit.Assert.assertEquals;

public class ExampleOhosTest {
    private static IAbilityDelegator sAbilityDelegator = AbilityDelegatorRegistry.getAbilityDelegator();

    @Test
    public void testBundleName() {
        final String actualBundleName = AbilityDelegatorRegistry.getArguments().getTestBundleName();
        assertEquals("yellow5a5.sample", actualBundleName);
    }

    @Test
    public void testMail() {
        Ability ability = EventHelper.startAbility(FirstActivity.class);
        goToSleep(3000);
        Image email = (Image) ability.findComponentById(ResourceTable.Id_fab);
        goToSleep(3000);
        EventHelper.triggerClickEvent(ability, email);
        goToSleep(3000);
        try {
            Class<ActSwitchAnimTool.SwitchAnimView> actSwitchAnimToolClass = ActSwitchAnimTool.SwitchAnimView.class;
            Method method = actSwitchAnimToolClass.getMethod("startShrinkAnim");
            ActSwitchAnimTool invoke = (ActSwitchAnimTool) method.invoke(actSwitchAnimToolClass);
            invoke.setShrinkBack(true);
            Assert.assertTrue(invoke.isShrinkBack());
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testBackMail() {
        goToSleep(3000);
        Ability ability = EventHelper.startAbility(FirstActivity.class);
        goToSleep(3000);
        Image email = (Image) ability.findComponentById(ResourceTable.Id_fab);
        goToSleep(3000);
        EventHelper.triggerClickEvent(ability, email);
        goToSleep(3000);
        SecondActivity secondActivity = (SecondActivity) sAbilityDelegator.getCurrentTopAbility();
        EventHelper.triggerKeyEvent(secondActivity, KeyEvent.KEY_BACK);
        goToSleep(3000);

        try {
            Class<ActSwitchAnimTool.SwitchAnimView> actSwitchAnimToolClass = ActSwitchAnimTool.SwitchAnimView.class;
            Method method = actSwitchAnimToolClass.getMethod("startSpreadAnim");
            ActSwitchAnimTool invoke = (ActSwitchAnimTool) method.invoke(actSwitchAnimToolClass);
            Assert.assertTrue(invoke.isWaitingResume());
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Test
    public void testShare() {
        goToSleep(3000);
        Ability ability = EventHelper.startAbility(FirstActivity.class);
        goToSleep(3000);
        Image email = (Image) ability.findComponentById(ResourceTable.Id_share_float_btn);
        goToSleep(3000);
        EventHelper.triggerClickEvent(ability, email);
        goToSleep(3000);

        try {
            Class<ActSwitchAnimTool.SwitchAnimView> actSwitchAnimToolClass = ActSwitchAnimTool.SwitchAnimView.class;
            Method method = actSwitchAnimToolClass.getMethod("startSpreadAnim");
            ActSwitchAnimTool invoke = (ActSwitchAnimTool) method.invoke(actSwitchAnimToolClass);
            Assert.assertTrue(invoke.isWaitingResume());
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testbackShare() {
        goToSleep(3000);
        Ability ability = EventHelper.startAbility(FirstActivity.class);
        goToSleep(3000);
        Image email = (Image) ability.findComponentById(ResourceTable.Id_share_float_btn);
        goToSleep(3000);
        EventHelper.triggerClickEvent(ability, email);
        goToSleep(3000);

        FirstActivity firstActivity = (FirstActivity) sAbilityDelegator.getCurrentTopAbility();
        Image backCancle = (Image) firstActivity.findComponentById(ResourceTable.Id_img_cancel);
        goToSleep(3000);
        EventHelper.triggerClickEvent(firstActivity, backCancle);
        goToSleep(3000);


    }


    private void goToSleep(long s) {
        try {
            Thread.sleep(s);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}