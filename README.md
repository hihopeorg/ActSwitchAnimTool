# ActSwitchAnimTool

---

**本项目是基于开源项目ActSwitchAnimTool进行ohos化的移植和开发的，可以通过项目标签以及github地址（https://github.com/Yellow5A5/ActSwitchAnimTool ）追踪到原项目版本**

#### 项目介绍

- 项目名称：ActSwitchAnimTool
- 所属系列：ohos的第三方组件适配移植
- 功能：
  圆形动画放大缩小转场动画
- 项目移植状态：完成
- 调用差异：无
- 项目作者和维护人：hihope
- 联系方式：hihope@hoperun.com
- 原项目Doc地址：https://github.com/Yellow5A5/ActSwitchAnimTool 
- 原项目基线版本: v1.0.1 sha1:32d0872cba962d2391f3504f687bec901a5a3699 
- 编程语言：Java
- 外部库依赖：无

## 效果展示

![](ActSwitchAnimTool.gif)

#### 安装教程

#### 方法一：

1. 编译ActSwitchAnimTool的har包ActSwitchAnimTool.har。

2. 启动 DevEco Studio，将编译的har包，在工程目录新建“libs”存放到下面。

3. 在工程级别下的build.gradle文件中添加:

  ```
 allprojects {
     repositories {
         maven {
             url 'https://repo.huaweicloud.com/repository/maven/'
         }
         maven {
             url 'https://developer.huawei.com/repo/'
         }
 		jcenter()
     flatDir {
         dir'../libs/'
     }
     }
 }
  ```

4.在需要引用har包的目录下

   ```
implementation fileTree(dir: 'libs', include: ['*.jar','*.har'])
api(name:"ActSwitchAnimTool-release",ext:"har")
   ```

#### 方法二:

1. 在工程的build.gradle的allprojects中，添加HAR所在的Maven仓地址:

```
 repositories {
     maven {
         url 'http://106.15.92.248:8081/repository/Releases/' 
     }
 }
```

2. 在应用模块的build.gradle的dependencies闭包中，添加如下代码:

```
 dependencies {
     implementation 'yellow5a5.actswitchanimtool.ohos:ActSwitchAnimTool:1.0.0'
 }
```

#### 使用说明

1.在代码中使用:

```
new ActSwitchAnimTool(this)
        .setAnimType(ActSwitchAnimTool.MODE_SPREAD)
        .target(mDemoFloatingBtn)
        .setmColorStart(Color.parseColor("#FF5777"))
        .setmColorEnd(Color.parseColor("#FF5777"))
        .setCustomEndCallBack(new ActSwitchAnimTool.SwitchAnimCallback() {
            @Override
            public void onAnimationStart() {
            }

            @Override
            public void onAnimationEnd() {
                finish();
            }

            @Override
            public void onAnimationUpdate(int progress) {

            }
        })
        .build();
        
        //在这里取你的自定义视图，以实现你想要的效果。
            mShareContainer = new ShareContainer(FirstActivity.this);
        mShareContainer.setIShareCallback(new ShareContainer.IShareCallback() {
            @Override
            public void onCancel() {
                mShareContainer.hideShareBtn();
                shareDemoTool.setAnimType(ActSwitchAnimTool.MODE_SHRINK)
                        .removeContainerView(mShareContainer)
                        .build();
            }
        });
        mShareViewDemoBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shareDemoTool.setAnimType(ActSwitchAnimTool.MODE_SPREAD)
                        .addContainerView(mShareContainer, new ActSwitchAnimTool.SwitchAnimCallback() {
                            @Override
                            public void onAnimationStart() {

                            }

                            @Override
                            public void onAnimationEnd() {
                                mShareContainer.showShareBtn();
                            }

                            @Override
                            public void onAnimationUpdate(int progress) {

                            }
                        }).
                        build();
            }
        });
```

#### 版本迭代

- v1.0.0
- 实现功能:
  1. 转场动画的放大
  2. 转场动画的缩小

#### 版权和许可信息

    Copyright 2016 Yellow5A5
    
    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at
    
        http://www.apache.org/licenses/LICENSE-2.0
    
    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

